from unicodedata import name
from django.urls import path
from . import views

urlpatterns=[
    path('', views.index, name='index'),
    path('available/list/', views.book_list, name='book_list'),
    path('borrowed-books/list/', views.borrowed_book_list, name='borrowed_book_list'),
    path('issue-book/list/', views.issue_book, name='issue_book'),
    path('add_book/', views.add_book, name='add_book'),
    path('view_books/list/', views.view_books, name='view_books'),
    path('update/<int:book_pk>/book', views.update_book, name='update'),
    path('register', views.register, name='register'),
    path('books/list/', views.all_books, name='books'),
    path('dashboard', views.dashboard, name='dashboard'),
    path('login', views.login_page, name='login'),
    path('sign-up', views.student_sign_up, name='sign'),
    path('thanks', views.thanks, name='thanks'),
    path('borrow', views.borrow, name='borrow'),
    path('logout', views.logout_user, name='logout'),
    path('payment_list', views.payment_list, name='payment_list'),
    path('category', views.category, name='category'),
    path('category_list', views.category_list, name='category_list'),
    path('delete/<int:pk>/book', views.delete_book, name='delete_book'),
    path('search', views.search_book, name='search'),
]