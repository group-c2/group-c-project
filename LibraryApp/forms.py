from django.forms import ModelForm
from django import forms
from . models import *
from django.db.models import Q
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm



class AddBookForm(forms.ModelForm):
    class Meta:
        model=Book
        fields = {'book_cover','book_name','isbn_number', 'book_author','category_name','year'}

class SignUpForm(UserCreationForm):
   

    class Meta:
        model=Student
        fields = {'first_name','middle_name','second_name','email','username','password1','password2','student_ID','phone_number'}

class BorrowBook(forms.ModelForm):
    class Meta:
        model=Borrow
        fields = {'student_name','book_name','return_date','issued'}
       
class StudentLogin(forms.ModelForm):
    class Meta:
        model=Student
        fields = {'username','password'}

class CategoryForm(forms.ModelForm):
    class Meta:
        model=Category
        fields ={ 'name', 'description'} 


class UpdateBookForm(forms.ModelForm):
    class Meta:
        model=Book
        fields ={'book_cover','book_name','isbn_number', 'book_author','category_name','year'}


    
