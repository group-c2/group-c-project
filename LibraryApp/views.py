import http
from multiprocessing import context
from django.shortcuts import redirect, render, get_object_or_404
from django.http import HttpResponse
from.models import *
from .forms import *
from django.http import HttpResponseRedirect,HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import messages



def dashboard(request):
    return render(request,'dashboard.html')
def login_page(request):
    logout(request)
    if request.method == 'POST':
        email=request.POST.get('email')
        password=request.POST.get('password')
        print(email)
        print(password)
        user=authenticate(email=email, password=password)
        print(user)
        if user is not None:
            if user.is_active:
                login(request, user)
                print('user is logged in')
                if user.is_superuser:
                    return redirect('dashboard')
                else:
                    return redirect('books')
    return render(request, 'login.html')

# Create your views here.
def index(request):
    
    return render(request, 'firstpage.html')

def register(request):
    return render(request, 'welcome.html')

def all_books(request):
    all=Book.objects.all()
    return render(request, 'books.html',{'all':all})


def  book_list(request):
    books = Book.objects.filter(book_status="1")
    context={'returned_books': books}
    return render(request, 'available_books.html', context)

def borrowed_book_list(request):
    borrowed = Borrow.objects.all()
    context={'borrowed_books': borrowed}
    return render(request, 'borrowed.html',context)


def payment_list(request):
    return render(request, 'payment.html')

def logout_user(request):
    logout(request)
    return redirect('index')
def add_book(request):
    if request.method =='POST':
        forms = AddBookForm(request.POST, request.FILES)
        if forms.is_valid():
            forms.save()
            return redirect('books')
    form=AddBookForm
    return render(request, 'addbook.html', {'form':form})

def view_books(request):
    return render(request, 'viewbooks.html')

def update_book(request, book_pk):
    book = get_object_or_404(Book, pk=book_pk)
    form = UpdateBookForm(instance=book)
    if request.method == "POST":
        form = UpdateBookForm(request.POST, request.FILES, instance=book)
        print(form.errors)
        if form.is_valid():
            messages.success(request, "Book Updated")
            return redirect('book_list')


    return render(request, 'update.html', {'book':book,'form':form})


def payment_list(request):
    return render(request, 'payment.html')

def thanks(request):
    name = Borrow.objects.all()
    context={'student_borrowing':name}
    return render(request, 'thanks.html', context)

def student_sign_up(request):
    if request.method =='POST':
        forms = SignUpForm(request.POST)
        print(forms.errors)
        if forms.is_valid():
            forms.save()
            return redirect('login')
    form=SignUpForm
    return render(request, 'student_signup.html', {'form':form})


def borrow(request):
    if request.method =='POST':
        forms = BorrowBook(request.POST)
        print(forms.errors)
        if forms.is_valid():
            forms.save()
            return redirect('payment_list')
    form=BorrowBook
    return render(request, 'borrow-book.html', {'form':form})

    
def studentlogin(request):

    
    return render(request, 'login.html', {'form':form})



def hello(request):
    return render(request, 'hello.html')


def issue_book(request):
    #it should connect to student request,student borrowing history
    issue = Borrow.objects.all()
    context = {'issue_book': issue}

    if request.method == "POST":
        update_issue = Borrow.objects.get(id=request.POST.get("id"))
        form = BorrowBook({
            "book_name" : update_issue.book_name,
            "student_name": update_issue.student_name,
            "return_date": update_issue.return_date,
            "issued": "1",
        }, instance=update_issue)

        if form.is_valid():
          form.save()
          messages.success(request,"The book has been issued")
        else:
          messages.error(request,form.errors)
          print(form.errors)

    return render(request, 'issue_book.html',context)


def category(request):
    if request.method =='POST':
        forms = CategoryForm(request.POST)
        print(forms.errors)
        if forms.is_valid():
            forms.save()
            return redirect('category_list')
    form=CategoryForm
    return render(request, 'category.html', {'form':form})

def category_list(request):
    category = Category.objects.all()
    context={'category_list': category}
    return render(request, 'category_list.html',context)
    
def delete_book(request,pk):
    book = get_object_or_404(Book, id=pk)
    if request.method == "GET":
        book.delete()
        messages.success(request, 'Book Deleted')
        return redirect("book_list")
    context ={'book':book}
    return render(request, 'delete_book.html', context)

def search_book(request):
    if request.method == 'POST':
        item = request.POST.get('search')
        books = Book.objects.filter(book_name__icontains= item)
        context = {'books':books}
        return render(request,'search.html', context)
    else:
        return render(request,'search.html')
