# Generated by Django 4.0.6 on 2022-08-18 10:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('LibraryApp', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='book',
            name='book_ID',
        ),
        migrations.RemoveField(
            model_name='borrow',
            name='book_id',
        ),
        migrations.AddField(
            model_name='book',
            name='book_cover',
            field=models.ImageField(blank=True, null=True, upload_to='images'),
        ),
        migrations.AddField(
            model_name='book',
            name='isbn_number',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='borrow',
            name='issued',
            field=models.CharField(blank=True, choices=[('1', 'Issued'), ('2', 'Not Issued')], default=2, max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='book_status',
            field=models.CharField(choices=[('1', 'Returned'), ('2', 'Taken')], default=1, max_length=2),
        ),
        migrations.AlterField(
            model_name='borrow',
            name='date_of_borrowing',
            field=models.DateField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='student',
            name='first_name',
            field=models.CharField(blank=True, default='', max_length=200),
        ),
        migrations.AlterField(
            model_name='student',
            name='middle_name',
            field=models.CharField(blank=True, default='', max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='student',
            name='password',
            field=models.CharField(max_length=128, verbose_name='password'),
        ),
        migrations.AlterField(
            model_name='student',
            name='phone_number',
            field=models.CharField(blank=True, max_length=30),
        ),
        migrations.AlterField(
            model_name='student',
            name='second_name',
            field=models.CharField(blank=True, default='', max_length=200),
        ),
        migrations.AlterField(
            model_name='student',
            name='username',
            field=models.CharField(blank=True, default='', max_length=200),
        ),
    ]
