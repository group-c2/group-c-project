from audioop import maxpp
from datetime import datetime
from random import choices
from django.db.models.deletion import CASCADE
from unicodedata import category
from django.db import models
from django.contrib.auth.models import AbstractUser
import datetime
from django.contrib.auth.models import User
# Create your models here.
#Student
#Borrow
#Category
#Book

class Student(AbstractUser):
    first_name=models.CharField(max_length=200,default='',blank=True,null=False)
    middle_name=models.CharField(max_length=200,blank=True,null=True,default='')
    second_name=models.CharField(max_length=200,default='',blank=True,null=False)
    email=models.EmailField(unique=True)
    username=models.CharField(max_length=200,default='',blank=True,null=False)
    student_ID = models.IntegerField(blank=True, null=True)
    phone_number = models.CharField(max_length=30,blank=True,null=False)
    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __unicode__(self):
        return self.email

    

class Category(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    def __str__(self):
        return self.name 

class Book(models.Model):
    book_cover = models.ImageField(upload_to="images", null=True, blank=True)
    category_name = models.ForeignKey(Category, on_delete=models.CASCADE)
    book_name = models.CharField(max_length=200)
    book_author = models.CharField(max_length=200)
    description = models.TextField(default="details of a book")
    isbn_number= models.CharField(max_length=20, blank=True, null=False)
    book_status=models.CharField(max_length=2, choices=(('1','Returned'), ('2','Taken')), default=1)
    year = models.CharField(max_length = 255, null=True, blank=True)
    

    def __str__(self):
        return self.book_name   

class Borrow(models.Model): 
    student_name = models.CharField(max_length=200,default='')
    book_name = models.ForeignKey(Book, on_delete=models.CASCADE)
    date_of_borrowing = models.DateField(auto_now_add=True)
    return_date = models.DateField()
    borrow_book=models.CharField(max_length=20,choices=(('1','Yes'), ('2','No')), default="Yes")
    fine = models.FloatField(default=0.0, blank=True, null=True) 
    issued=models.CharField(max_length=20,null=True, blank=True, choices=(('1','Issued'), ('2','Not Issued')), default=2)
    def __str__(self):
        return self.student_name


    





    