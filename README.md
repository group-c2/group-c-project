
Library Management System

A Library Management System is software that provides the ability to find books, manage books, track borrowed books, managing fines and bills all in one place. It helps the librarian manage the books and books borrowed by members and automates most of the library activities. It increases efficiency and reduces the cost needed for maintaining a library and saves time and effort for both the user and the librarian.

System Requirements

The key requirements that need to be offered by the library managemant system can be functional and non-functional requirements.

Functional requirements

Allow the librarian to add and remove new members.

Allow the user to search for books based on title, publication date, author, etc., and find their location in the library.

Users can request, reserve, or renew a book.

Librarian can add and manage the books.

The system should notify the user and librarian about the overdue books.

The system calculates the fine for overdue books on their return.

User - The user can log in, view the catalog, search for books, checkout, reserve, renew and return a book.

Librarian - The librarian registers new users, adds and maintains the books, collects fines for overdue books, and issues books to users who need them.

System - The system is the library management system itself. It keeps track of the borrowed books and sends notifications to the user and librarian about the overdue books

Non functional requirements

Usability:
 The UI should be simple enough for everyone to understand and get the relevant information without any special training. Different languages can be provided based on the requirements.

Accuracy:
 The data stored about the books and the fines calculated should be correct, consistent, and reliable.

Availability:
The System should be available for the duration when the library operates and must be recovered within an hour or less if it fails. The system should respond to the requests within two seconds or less.

Maintainability:
The software should be easily maintainable and adding new features and making changes to the software must be as simple as possible. In addition to this, the software must also be portable

Software Requirements:

A server running Windows Server/Linux OS

A multi-threading capable backend language like python

Front-end frameworks like Angular/react/Vue for the client


